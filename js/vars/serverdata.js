guessmynumber.vars.server = function(_options){
	var options = {
		"server":"http://guessmynumber.jurgens.com.ar",
		"port":"80"
	}

	for(var option in _options) {//Esta parte solo deberia ser necesaria si la crease como objeto o la llamase pasandole los parametros. La dejo para futura referencia.
		options[option] = _options[option];
	}

	return {
		"setServer":function(server) {
			options.server = server;
		},
		"getServer":function() {
			return options.server;
		},
		"setPort":function(port) {
			options.port = port;
		},
		"getPort":function() {
			return options.port;
		}
	}
}();/*Auto ejecucion para que se guarde*/
