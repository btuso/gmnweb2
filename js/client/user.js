guessmynumber.client.user = function(_options){
	var data = {
		"username":"default",
		"privateUuid":"",
		"numberHistory":{}
	}

	var boardTimer, started = false;

	for(var option in _options) {
		data[option] = _options[option];
	}
	
	var refreshBoard = function() {
		guessmynumber.functions.ajax({"parameters":"/players/board/"+data.privateUuid},function(_message,_status){
			switch(_status){
				case 200:
					$("#username-p").text("Usuario: "+data.username+" - Puntaje:"+_message.me[0].score);
					if(_message.me[0].numberActivated == false){
						stop();
						$("#oponents").empty();
						guessmynumber.functions.switchDivs($("#setNumber-div"));
						guessmynumber.functions.errorHandler({"message":"Tu numero fue adivinado, elegi otro para poder seguir jugando"});
						$("#ownNumber").val(guessmynumber.functions.randomNumber());
					}else{
						if(!guessmynumber.client.play.isActive()){
							var source = $("#oponents-script").html();
							var template = Handlebars.compile(source);
							for(var i = _message.players.length-1;i>=0; i--){
								if(_message.players[i].numberActivated == false)
									_message.players.splice(i,1);
								else
									_message.players[i].publicUuid = _message.players[i].publicUuid.replace(/-/g,"");
							}
							if(_message.players.length > 0){
								_message.players.sort(function(a,b){return b.score-a.score});//Pongo primero al que tiene el mayor puntaje
								var html = template(_message);
							} else {
								var html = '<h5>No hay ningun jugador con un numero activo. \n Espera a que aparezca alguno para poder jugar o clickea el boton para generar un oponente al azar.<h5><br /><input type="button" onClick="guessmynumber.functions.generateOponent()" value="Generar oponente">';
							}
							$("#oponents").html(html);
						} else {
							var oponent = "none";
							for(var i = 0;i<_message.players.length;i++){
								if(_message.players[i].publicUuid.replace(/-/g,"") == guessmynumber.client.play.getOponent())
									oponent = i;
							}
							if(oponent != "none"){//Checkeo por si resetearon server
								if(!_message.players[oponent].numberActivated){
									guessmynumber.client.play.stop();
									guessmynumber.functions.errorHandler({"message":"El numero de tu oponente fue adivinado, elegi otro para poder seguir jugando"});									
								}else{
									$("#oponent-score").text(_message.players[oponent].score);
								}
							} else {
								guessmynumber.client.play.stop();
								guessmynumber.functions.errorHandler({"message":"Tu oponente fue eliminado del servidor, elegi otro para poder seguir jugando"});
							}
						}
					}
					break;
				case 521://Si no se encuentra el ID es porque se reinicio el server, autoregistrarse.(En el proximo boardGet se va a pedir el numero como si hubiese sido adivinado)
					guessmynumber.functions.ajax({"parameters":"/players/register/"+data.username},function(_message,_status){						
						data.privateUuid = _message.privateUuid;
					});
			}
			if(started)
				boardTimer = setTimeout(refreshBoard, 1000);
		});
	}

	var start = function() {
		started = true;
		boardTimer = setTimeout(refreshBoard, 1000);
	}

	var stop = function() {
		started = false;
		clearTimeout(boardTimer);
	}

	return {
		"setUsername":function(_username) {
			data.username = _username;
		},
		"getUsername":function() {
			return data.username;
		},
		"setUuid":function(_privateUuid) {
			data.privateUuid = _privateUuid;
		},
		"getUuid":function() {
			return data.privateUuid;
		},
		"getNumberHistory":function(_number){
			if(!data.numberHistory[_number])
				data.numberHistory[_number] = {"attempt":[],"won":{"status":false,"number":""}};
			return data.numberHistory[_number];
		},
		"pushNumberHistory":function(_number,_result){
			if(!data.numberHistory[_number])
				data.numberHistory[_number] = {"attempt":[],"won":{"status":false,"number":""}};
			_result.n = data.numberHistory[_number].attempt.length + 1;
			data.numberHistory[_number].attempt.unshift(_result);//Uso unshift para que me queden ordenados bien los intentos

		},
		"winNumber":function(_numberId,_number){
			data.numberHistory[_numberId].won = {"status":true,"number":_number};//Lo seteo como adivinado para poner como value en el input
		},
		"start":start,
		"stop":stop
	}
};
