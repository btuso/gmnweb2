guessmynumber.client.play = function(){
	var data = {
		"oponent":"none",
		"number":"none"
	}
	
	var start = function(_oponent,_number){
		data.oponent = _oponent;
		data.number = _number;
		$("#oponents").empty();
		$("#oponent-image").attr({"src":"http://www.gravatar.com/avatar/"+_oponent+"?d=retro"});
		$("#top-bar-left").hover(function(){
						$("#top-bar-left").css("background-image","url('./images/top-bar-left-return-h.png')")
					},function(){
						$("#top-bar-left").css("background-image","url('./images/top-bar-left-return.png')")
					}).css("background-image","url('./images/top-bar-left-return.png')").click(function(){
						stop();
					});
		guessmynumber.functions.switchDivs($("#play-div"));
		if(user.getNumberHistory(data.number).won.status == true)
			$("#guessingNumber").val(user.getNumberHistory(data.number).won.number);
		else
			$("#guessingNumber").val(guessmynumber.functions.randomNumber());
		updateTable();
	}
	
	var stop = function(){
		data.oponent = "none";
		data.number = "none";
		$("#numberHistory").empty();
		$("#oponents").empty();
		$("#top-bar-left").unbind('mouseenter mouseleave click').css("background-image","url('./images/top-bar-left.png')");
		guessmynumber.functions.switchDivs($("#oponents-div"));
	}
	
	var updateTable = function(){
		var source = $("#history-script").html();
		var template = Handlebars.compile(source);
		var html = template(user.getNumberHistory(data.number));
		$("#numberHistory").html(html);
	}
	
	return{
		"isActive":function(){
				if(data.oponent != "none")
					return true;
				return false;
			},
		"getOponent":function(){
				return data.oponent;
			},
		"start":start,
		"stop":stop,
		"resume":function(){
				start(data.oponent,data.number)
			},
		"updateTable":updateTable
	}
}();//Auto ejecucion para que quede en memoria
