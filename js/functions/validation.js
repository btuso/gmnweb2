guessmynumber.functions.checkServer = function(){//Funcion tomada del proyecto en grupo, hecha por Ariel.
	$.ajax({
	type: "GET",
	timeout: 2000,
	url: $("#server").val() + ':' + $("#port").val() + '/version',
	async: true,
	success: function(data,status,xhr){
		if(xhr.readyState == 4)
			if(xhr.status == 200){
				$("#server").css({'background-color' : '#6EDA60'});
				$("#port").css({'background-color' : '#6EDA60'});
			}
	},
	error: function(jqXHR){
		if(jqXHR.readyState <= 0){
				$("#server").css({'background-color' : '#FF6666'});
				$("#port").css({'background-color' : '#FF6666'});
			}
	}
});
}

guessmynumber.functions.validateServer = function(){
	if ($("#server").val().indexOf("http://") !== 0)
		$("#server").val("http://" + $("#server").val());
	if($("#server").val().substring($("#server").val().length - 1) == "/")
		$("#server").val($("#server").val().substring(0, $("#server").val().length - 1));
}

guessmynumber.functions.validatePort = function(evt){
	var keyPressed = (evt.which) ? evt.which : evt.keyCode
	return !(keyPressed > 31 && (keyPressed < 48 || keyPressed > 57));
}
