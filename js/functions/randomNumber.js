guessmynumber.functions.randomNumber = function(){
	var mask = [0,1,2,3,4,5,6,7,8,9];
	var chosencode = "";		//Elijo el numero usando un array como mascara
	for(var i = 0;i<4;i++){		//para asegurar que no haya caracteres repetidos
		var rnd = mask[Math.floor(Math.random()*(10-i))];
		chosencode += rnd;
		mask.splice(mask.indexOf(rnd),1);
	}
	
	return chosencode;
}
