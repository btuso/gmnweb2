guessmynumber.functions.errorHandler = function(_options){
              options = {
                 "message" : "Se produjo un error no identificado, recarga la pagina.",
                 "color" : "#D62300"
              }

              for(var option in _options)
                  options[option] = _options[option];
			  $(".error-msg").css("color",options.color);
              $(".error-msg").show();
              $(".error-msg").text(options.message);
          }
