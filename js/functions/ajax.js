guessmynumber.functions.ajax = function(_options, _callback) {
    var options = {
		"parameters" : "/version"
	}

	for(var option in _options) {
		options[option] = _options[option];
	}
        
    $.ajax({
        "url": guessmynumber.vars.server.getServer() + ":" + guessmynumber.vars.server.getPort() + options.parameters,
        "complete": function(data) {
            var msg = data.responseText ? JSON.parse(data.responseText) : {error:'0', message:'unhandled error'};
            _callback(msg,data.status);
        }
    })
};
